#!/bin/bash

# use debootstrap to create a base layer.

if [ -d stage0 ]; then
	rm -rf stage0
fi
if [ -f stage0.tar.gz ]; then
	rm -rf stage0.tar.gz
fi

mkdir stage0
debootstrap --arch amd64 --include build-essential,texinfo,gawk,bison,python3,rust-all,git,tar,ca-certificates,wget,xz-utils stable stage0/ http://deb.debian.org/debian/
rm -rf stage0/dev
rm -f stage0/bin/sh
ln -s /usr/bin/bash stage0/bin/sh


casupload --cas-server=http://localhost:50040 stage0


